# coding=utf-8
"""Testing del módulo **bencoding**.

.. module:: tests.test_bencoding
   :platform: Unix, Windows
   :synopsis: Testing del módulo bencoding.
.. moduleauthor:: Diego J. Valdeolmillos Villaverde

"""
import unittest

from bitTorrent import bencoding


class BencodingDecodingTest(unittest.TestCase):
    """Clase que testea las siguientes funciones:

        - decode_int
        - decode_string
        - decode_list
        - decode_dictionary
        - decode_element
    """
    TEST_VALUES_INT = {'i32e': (32, 4), 'i-12e': (-12, 5)}
    TEST_VALUES_STRING = {'6:python': ('python', 8), '3:tfg': ('tfg', 5), '3:tfgz': ('tfg', 5)}
    TEST_VALUES_LIST = {'l5:item15:item2i-2ee': (['item1', 'item2', -2], 20), 'l4:spami32ee': (['spam', 32], 12),
                        'le': ([], 2)}
    TEST_VALUES_DICTIONARY = {
        'd3:cow3:moo4:spaml4:eggsi44ee3:cati1ee': ({'cow': 'moo', 'spam': ['eggs', 44], 'cat': 1}, 38),
        'de': ({}, 2),
        'd9:publisher3:bob16:publisherwebpage15:www.example.com18:publisher.location4:homee':
            ({'publisher': 'bob', 'publisherwebpage': 'www.example.com', 'publisher.location': 'home'}, 82)}

    def test_decoding_integer(self):
        """Test de decodificación de enteros.
        """
        for k, v in self.TEST_VALUES_INT.items():
            self.assertEqual(bencoding.decode_integer(k, 0), v)

    def test_decoding_string(self):
        """Test de decodificación de cadenas.
        """
        for k, v in self.TEST_VALUES_STRING.items():
            self.assertEqual(bencoding.decode_string(k, 0), v)

    def test_decoding_list(self):
        """Test de decodificación de listas.
        """
        for k, v in self.TEST_VALUES_LIST.items():
            self.assertEqual(bencoding.decode_list(k, 0), v)

    def test_decoding_dictionary(self):
        """Test de decodificación de diccionarios.
        """
        for k, v in self.TEST_VALUES_DICTIONARY.items():
            decode, index = bencoding.decode_dictionary(k, 0)
            self.assertEqual(index, v[1])
            self.assertDictEqual(decode, v[0])

    def test_decoding_element(self):
        """Test de decodificación de elementos.

           Un elemento puede ser:


                    - entero (int).
                    - cadena (str).
                    - lista (list).
                    - diccionario (dict).
        """
        for k, v in self.TEST_VALUES_INT.items():
            self.assertEqual(bencoding.decode_element(k, 0), v)
        for k, v in self.TEST_VALUES_STRING.items():
            self.assertEqual(bencoding.decode_element(k, 0), v)
        for k, v in self.TEST_VALUES_LIST.items():
            self.assertEqual(bencoding.decode_element(k, 0), v)
        for k, v in self.TEST_VALUES_DICTIONARY.items():
            self.assertEqual(bencoding.decode_element(k, 0), v)


class BencodingEncodingTest(unittest.TestCase):
    """Clase que testea las siguientes funciones:

        - encode_int
        - encode_string
        - encode_list
        - encode_dictionary
        - encode_element
    """
    TEST_VALUES_INT = {32: 'i32e', -12: 'i-12e'}
    TEST_VALUES_STRING = {'test': '4:test', 'tfg': '3:tfg'}
    TEST_VALUES_LIST = {'l4:testi3ee': ['test', 3], 'l4:spami32ee': ['spam', 32], 'le': []}
    TEST_VALUES_DICTIONARY = {
        'd3:cow3:moo4:spaml4:eggsi44ee3:cati1ee': {'cow': 'moo', 'spam': ['eggs', 44], 'cat': 1},
        'de': {},
        'd9:publisher3:bob17:publisher-webpage15:www.example.com18:publisher.location4:homee':
            {'publisher': 'bob', 'publisher-webpage': 'www.example.com', 'publisher.location': 'home'}}

    def test_encoding_int(self):
        """Test de codificación de enteros.
        """
        for k, v in self.TEST_VALUES_INT.items():
            self.assertEqual(bencoding.encode_integer(k), v)

    def test_encoding_string(self):
        """Test de codificación de cadenas.
        """
        for k, v in self.TEST_VALUES_STRING.items():
            self.assertEqual(bencoding.encode_string(k), v)

    def test_encoding_list(self):
        """Test de codificación de listas.
        """
        for k, v in self.TEST_VALUES_LIST.items():
            self.assertEqual(bencoding.encode_list(v), k)

    def test_encoding_dictionary(self):
        """Test de codificación de diccionarios.
        """
        for k, v in self.TEST_VALUES_DICTIONARY.items():
            dictionary = bencoding.decode_dictionary(k, 0)[0]
            string = bencoding.encode_dictionary(v)
            dictionary2 = bencoding.decode_dictionary(string, 0)[0]
            self.assertDictEqual(dictionary, dictionary2)

    def test_element(self):
        """Test de codificación de elementos.

            Un elemento puede ser:


                - entero (int).
                - cadena (str).
                - lista (list).
                - diccionario (dict).
        """
        for k, v in self.TEST_VALUES_INT.items():
            self.assertEqual(bencoding.encode_integer(k), v)
        for k, v in self.TEST_VALUES_STRING.items():
            self.assertEqual(bencoding.encode_string(k), v)
        for k, v in self.TEST_VALUES_LIST.items():
            self.assertEqual(bencoding.encode_list(v), k)
        for k, v in self.TEST_VALUES_DICTIONARY.items():
            dictionary = bencoding.decode_dictionary(k, 0)[0]
            string = bencoding.encode_dictionary(v)
            dictionary2 = bencoding.decode_dictionary(string, 0)[0]
            self.assertDictEqual(dictionary, dictionary2)


if __name__ == '__main__':
    unittest.main()

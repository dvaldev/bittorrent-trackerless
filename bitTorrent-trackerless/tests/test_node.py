# coding=utf-8
"""Testing de la clase **Node**.

.. module:: tests.test_node
   :platform: Unix
   :synopsis: Testing de la clase Node.
.. moduleauthor:: Diego J. Valdeolmillos Villaverde

"""
import unittest

from bitTorrent import dht
from bitTorrent import utils
import sys
from StringIO import  StringIO


class NodeTest(unittest.TestCase):
    """Clase que testea la clase Node, la cual contiene los siguientes métodos:

        - print_node
    """

    id_node = "3430796d6e6f39376d6b746d7036693870376472"
    ip = "127.0.0.1"
    port = 6881
    test_print_1 = 'id node : '
    test_print_2 = ' - ip addres : 127.0.0.1 - ip port : 6881 - ip node type : 0'
    id_node_hex = "33343330373936643665366633393337366436623734366437303336363933383730333736343732"

    def setUp(self):
        self.node = dht.Node(self.id_node, self.ip, self.port)
        self.saved_stdout = sys.stdout

    def test_print_node(self):
        """Test del método imprimir nodo .
        """
        try:
            out = StringIO()
            sys.stdout = out
            self.node.print_node()
            self.output = out.getvalue().strip()
            self.assertEqual(self.output, self.test_print_1 + self.id_node_hex + self.test_print_2)
        finally:
           pass

    def tearDown(self):
        sys.stdout = self.saved_stdout
        print ("output", type(self.output), self.output)
# coding=utf-8
"""Testing de la clase **DHT**.

.. module:: tests.test_DHT
   :platform: Unix
   :synopsis: Testing de la clase DHT.
.. moduleauthor:: Diego J. Valdeolmillos Villaverde

"""
import unittest

from bitTorrent import dht
from bitTorrent import  utils


class dhtTest(unittest.TestCase):
    """Clase que testea la clase Node, la cual contiene los siguientes métodos:

        - increment_transaction_id
        - generate_secret_for_token
        - remove_aceppted_token
        - generate_token
        - bootstrap
        - ping
        - get_peers
        - get_peers_recursive
        - sort_guid_by_distance
        - find_node
        - announce_peers
        - add_node
        - transaction_id_to_string 
    """
    def setUp(self):
        self.my_id = utils.generate_id()
        self.routing_table = dht.DHT(self.my_id)


    def test_transaction_id(self):
        """Test de incrementar transaction_id.
        """
        table = dht.DHT(self.my_id)
        before_transaction_id =table.transaction_id_to_string()
        table.increment_transaction_id()
        after_transaction_id = table.transaction_id_to_string()
        self.assertEqual(str(int(before_transaction_id) + 1), after_transaction_id)

    def test_generate_secret_for_token(self):
        """Test de comprobación de la generación de una clave para generar el token.
        """
        pass

    def test_remove_accepted_token(self):
        """Test de elimación del token recibido.
        """
        pass

    def test_generate_token(self):
        """Test de generación del token.
        """
        pass

    def test_bootstrap(self):
        """Test de bootstrap para la unión a la rede bittorrent.
        """
        pass

    def test_ping(self):
        """Test de envio del KRPC ping.
        """
        pass

    def test_get_peers(self):
        """Test de envio del KRPC get_peers.
        """
        pass

    def test_get_peers_recursive(self):
        """Test para añadir recursión sobre el KRPC get_peers y así obtener los nodos más cercanos.
        """
        pass

    def test_sort_guid_by_distance(self):
        """Test de ordenación de nodos por distancia.
        """
        pass

    def test_find_node(self):
        """Test del KRPC de find_node .
        """
        pass

    def test_announce_peers(self):
        """Test del KRPC de announce_peers.
        """
        pass

    def test_add_node(self):
        """Test de añadir un nodo a la tabla DHT.
        """
        pass

    def test_transaction_id_to_string(self):
        """Test de conversión el transaction_id a formato cadena.
        """
        pass


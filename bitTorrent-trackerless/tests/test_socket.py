# coding=utf-8
"""Testing de la clase **Socket**.

.. module:: tests.test_socket
   :platform: Unix
   :synopsis: Testing de la clase Socket.
.. moduleauthor:: Diego J. Valdeolmillos Villaverde

"""
import unittest
from bitTorrent import sockets
from bitTorrent import constants
import time
import os
import signal
import subprocess


class SocketTestTCP(unittest.TestCase):
    """Clase que testea la clase Socket (TCP), la cual contiene los siguientes métodos:

        - send
        - receive
        - close
    """
    ASSERT_TEXT = "test"

    def setUp(self):
        canfork = hasattr(os, 'fork')
        if not canfork:
            raise Exception("Platform does not support forking.")
        self.server_pid = None
        self.parent = os.fork()
        if self.parent:
            self.s_TCP_server = sockets.Socket("127.0.0.1", 6881, constants.TCP_TYPE, constants.SERVER)
            # os.wait()
            self.data_tcp = self.s_TCP_server.receive()
        else:
            time.sleep(1)
            self.s_TCP_client = sockets.Socket("127.0.0.1", 6881, constants.TCP_TYPE, constants.CLIENT)
            self.s_TCP_client.send(self.ASSERT_TEXT)

    def test_TCP(self):
        """Test de sockets TCP:
            - crear socket SOCK_STREAM
            - enviar
            - recibir
        """
        if self.parent:
            self.assertEqual(self.data_tcp, self.ASSERT_TEXT)
            self.s_TCP_server.close()
            self.s_TCP_server = None
            os.kill(self.parent, signal.SIGINT)
        else:
            self.s_TCP_client.close()
            self.s_TCP_client = None




class SocketTestUDP(unittest.TestCase):
    """Clase que testea la clase Socket (UDP), la cual contiene los siguientes métodos:

        - send
        - receive
        - close
    """
    ASSERT_TEXT = "test"

    def setUp(self):
        canfork = hasattr(os, 'fork')
        if not canfork:
            raise Exception("Platform does not support forking.")
        self.parent = os.fork()
        if self.parent:
            self.s_UDP_server = sockets.Socket("127.0.0.1", 6882, constants.UDP_TYPE, constants.SERVER)
            # os.wait()
            self.data_udp = self.s_UDP_server.receive()
        else:
            time.sleep(1)
            self.s_UDP_client = sockets.Socket("127.0.0.1", 6882, constants.UDP_TYPE, constants.CLIENT)
            self.s_UDP_client.send(self.ASSERT_TEXT)

    def test_UDP(self):
        """Test de sockets UDP:
            - crear socket SOCK_DGRAM
            - enviar
            - recibir
        """
        if self.parent:
            self.assertEqual(self.data_udp, self.ASSERT_TEXT)
            self.s_UDP_server.close()
            self.s_UDP_server = None
            os.kill(self.parent, signal.SIGINT)
        else:
            self.s_UDP_client.close()
            self.s_UDP_client = None

if __name__ == '__main__':
    unittest.main()

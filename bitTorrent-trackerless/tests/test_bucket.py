# coding=utf-8
"""Testing de la clase **Bucket**.

.. module:: tests.test_bucket
   :platform: Unix
   :synopsis: Testing de la clase Bucket.
.. moduleauthor:: Diego J. Valdeolmillos Villaverde

"""
import unittest

from bitTorrent import dht
from bitTorrent import constants
from bitTorrent import utils
import threading
import sys
from StringIO import StringIO


class BucketTest(unittest.TestCase):
    """Clase que testea la clase Bucket, la cual contiene los siguientes métodos:

        - add_node
        - bucket_is_not_full
        - bucket_is_leaf
        - delete_node
        - bucket_can_split
        - node_in_range
        - split_bucket
        - time_out
        - get_k_closest
        - sort_list_by_distance
        - print_bucket
    """
    ip = "127.0.0.1"
    port = "6881"
    identifier = utils.generate_id()
    number_of_buckets = [1]
    bucket = dht.Bucket(constants.MINSPACE, constants.MAXSPACE, identifier, number_of_buckets, True)

    def setUp(self):
        self.node = dht.Node(self.identifier, self.ip, self.port)
        self.saved_stdout = sys.stdout

    def test_add_node(self):
        """Test de adición de un nodo al bucket.
        """
        self.node = dht.Node(self.identifier, self.ip, self.port)
        self.bucket.add_node(self.node)
        self.assertEqual(self.bucket.count_nodes, 1)

    def test_bucket_is_not_full(self):
        """Test de comprobación si el bucket no está lleno.
        """
        self.assertEqual(self.bucket.bucket_is_not_full(), True)

    def test_bucket_is_leaf(self):
        """Test de comprobación de si el bucket es una hoja.
        """
        self.assertEqual(self.bucket.bucket_is_leaf(), True)

    def test_delete_node(self):
        """Test de eliminación de un nodo del bucket.
        """
        self.bucket.delete_node(self.node)
        self.assertEqual(self.bucket.count_nodes, 0)

    def test_bucket_can_split(self):
        """Test de comprobación de si el bucket se puede dividir.
        """
        self.assertEqual(self.bucket.bucket_can_split(), True)

    def test_node_in_range(self):
        """Test de comprobación de si un nodo es válido para el bucket.
        """
        self.assertEqual(self.bucket.node_in_range(self.identifier), True)

    def test_split_bucket(self):
        """Test de división de un bucket en 2 buckets.
        """
        for n in range(constants.K):
            node = dht.Node(utils.generate_id(), self.ip, self.port)
            self.bucket.add_node(node)
        self.bucket.split_bucket()
        self.assertEqual(self.bucket.number_of_buckets[0], 2)

    def test_timeout(self):
        """Test de comprobación del time out para el bucket.
        """
        self.assertEqual(type(self.bucket.scheduler), threading._Timer)

    def test_get_k_closest(self):
        """Test de obención de los 8 nodos más cercanos.
        """
        closest_node = []
        bucket2 = dht.Bucket(constants.MINSPACE, constants.MAXSPACE, self.identifier, self.number_of_buckets, True)
        for n in range(constants.K):
            node = dht.Node(utils.generate_id(), self.ip, self.port)
            bucket2.add_node(node)
        bucket2.get_k_closest(self.identifier, closest_node)
        self.assertEqual(len(closest_node), 8)

    def test_sort_list_by_distance(self):
        """Test de ordenación de los nodos por distancia respecto a un identificador dado.
        """
        array_nodes = []
        array_ids = [1, 2, 8, 4, 7, 9, 6, 3]
        array_ids_in_order = [1, 2, 3, 4, 6, 7, 8, 9]
        bucket2 = dht.Bucket(constants.MINSPACE, constants.MAXSPACE, str(0), self.number_of_buckets, True)
        for n in array_ids:
            node = dht.Node(str(n), self.ip, self.port)
            bucket2.add_node(node)
        for node_id, node in bucket2._kbuckets.items():
            array_nodes.append(node)
        self.bucket.sort_list_by_distance(array_nodes, str(0))
        for node, i in zip(array_nodes, array_ids_in_order):
            self.assertEqual(int(node.id_node), i)

    def test_print_bucket(self):
        """Test de impresión de un bucket.
        """
        try:
            out = StringIO()
            sys.stdout = out
            self.bucket.print_bucket()
            self.output = out.getvalue().strip()
            self.assertGreater(len(self.output), 0)
        finally:
            pass

    def tearDown(self):
        self.bucket.scheduler.cancel()
        sys.stdout = self.saved_stdout


if __name__ == '__main__':
    unittest.main()

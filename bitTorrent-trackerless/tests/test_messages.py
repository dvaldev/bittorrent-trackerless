# coding=utf-8
"""Testing del módulo **messages**.

.. module:: tests.test_messages
   :platform: Unix, Windows
   :synopsis: Testing del módulo messages.
.. moduleauthor:: Diego J. Valdeolmillos Villaverde

"""
import unittest
import ast
from bitTorrent import messages


class MessagesTest(unittest.TestCase):
    """Clase que testea las siguientes funciones:
    
        - message_ping
        - message_find_node
        - message_get_peers
        - message_announce_peers
    """

    PEER_ID = "abcdefghij0123456789"
    TRANSACTION_ID = "0f"
    NODE_TARGET = "mnopqrstuvwxyz123456"
    INFO_HASH = "mnopqrstuvwxyz123456"
    TOKEN = "aoeusnth"
    PORT = 6881
    TEST_VALUE_PING = "{'y':'q','q':'ping', 't': '0f' , 'a':{'id':'abcdefghij0123456789'}}"
    TEST_VALUE_FIND_NODE = "{'t':'0f', 'y':'q', 'q':'find_node'," \
                           " 'a': {'id':'abcdefghij0123456789', 'target':'mnopqrstuvwxyz123456'}}"
    TEST_VALUE_GEET_PEERS = "{'t':'0f', 'y':'q', 'q':'get_peers'," \
                            " 'a': {'id':'abcdefghij0123456789', 'info_hash':'mnopqrstuvwxyz123456'}}"
    TEST_VALUE_ANNOUNCE = "{'t':'0f', 'y':'q', 'q':'announce_peers'," \
                          " 'a': {'id':'abcdefghij0123456789'," \
                          " 'info_hash':'mnopqrstuvwxyz123456', 'port' : 6881, 'token' : 'aoeusnth'}}"

    def test_message_ping(self):
        """Test del KRPC ping .
        """
        self.assertDictEqual(ast.literal_eval(self.TEST_VALUE_PING),
                             messages.message_ping(self.PEER_ID, self.TRANSACTION_ID))

    def test_message_find_node(self):
        """Test del KRPC find_node .
        """
        self.assertDictEqual(ast.literal_eval(self.TEST_VALUE_FIND_NODE),
                             messages.message_find_node(self.PEER_ID, self.TRANSACTION_ID, self.NODE_TARGET))

    def test_message_geet_peers(self):
        """Test del KRPC geet_peers.
        """
        self.assertDictEqual(ast.literal_eval(self.TEST_VALUE_GEET_PEERS),
                             messages.message_get_peers(self.PEER_ID, self.TRANSACTION_ID, self.INFO_HASH))

    def test_message_announce(self):
        """Test del KRPC announce.
        """
        self.assertDictEqual(ast.literal_eval(self.TEST_VALUE_ANNOUNCE),
                             messages.message_announce_peers(self.PEER_ID, self.TRANSACTION_ID, self.INFO_HASH,
                                                             self.TOKEN, self.PORT))


if __name__ == '__main__':
    unittest.main()

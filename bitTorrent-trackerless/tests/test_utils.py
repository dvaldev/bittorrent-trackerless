# coding=utf-8
"""Testing del módulo **utils**.

.. module:: tests.test_utils
   :platform: Unix, Windows
   :synopsis: Testing del módulo utils.
.. moduleauthor:: Diego J. Valdeolmillos Villaverde

"""
import unittest

from bitTorrent import utils
from bitTorrent import constants
from bitTorrent import messages
from bitTorrent import sockets
from bitTorrent import bencoding


class TestUtils(unittest.TestCase):
    """Clase que testea las siguientes funciones:

        - hexadecimal_to_binary
        - sha1_string
        - generate_id
        - split_compact_node_info
        - split_compact_peer_info
    """
    test_values_sha1_string = {'test': 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3',
                               'hello world': '2aae6c35c94fcfb415dbe95f408b9ce91ee846ed'}
    ip_example = "127.0.0.1"
    port_example = "6881"

    def setUp(self):
        self.id_test = utils.generate_id()

    def test_hexadecimal_to_binary(self):
        """Test de conversión binario a decimal.
        """
        hexadecimal_str = self.id_test.encode('hex')
        self.assertEqual(utils.hexadecimal_to_binary(hexadecimal_str).__len__(), 160)

    def test_xor(self):
        """Test de la operación xor bit a bit.
        """
        self.assertEqual(utils.xor(self.id_test.encode('hex'), self.id_test.encode('hex')).decode('hex').__len__(), 20)
        self.assertEqual(utils.xor(self.id_test.encode('hex'), self.id_test.encode('hex')),
                         "0000000000000000000000000000000000000000")

    def test_sha1_string(self):
        """Test de aplicación de la función sha1 a una cadena.
        """
        for k, v in self.test_values_sha1_string.items():
            self.assertEqual(utils.sha1_string(k), v)

    def test_generate_id(self):
        """Test de generación de una cadena aleatoria para el identificador de nodo.
        """
        self.assertEqual(utils.generate_id().__len__(), 20)

    def test_split_compact_node_info(self):
        """Test de conversión de un nodo en formato compact_node_info (cadena de 26 bytes) a:
            (node_id, ip, port).
        """
        ip, port = constants.BOOTSTRAP[0]
        query = messages.message_find_node(self.id_test, "0f", self.id_test)
        s = sockets.Socket(ip, port, constants.UDP_TYPE, constants.CLIENT)
        s.send(bencoding.encode_element(query))
        r = s.receive()
        response = bencoding.decode_element(r, 0)[0]
        all_nodes = response["r"]["nodes"]
        nodes = utils.split_compact_node_info(all_nodes)
        self.assertGreater(len(nodes), 0)
        self.assertEqual(len(nodes[0][0]), 20)
        self.assertRegexpMatches(nodes[0][1], "^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$")
        self.assertRegexpMatches(str(nodes[0][2]),
                                 "^([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$")

    def test_split_compact_peer_info(self):
        """Test de conversión de un nodo en formato compact_peer_info (cadena de 6 bytes) a:
            (ip, port).
        """
        pass


if __name__ == '__main__':
    unittest.main()

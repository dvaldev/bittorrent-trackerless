# coding=utf-8
"""Constantes utilizadas por la aplicación.

.. module:: bitTorrent.constants
   :platform: Unix, Windows
   :synopsis: Constantes.
.. moduleauthor:: Diego J. Valdeolmillos Villaverde

"""

# routing table

K = 8  # size of bucket
ALPHA = 3  # number of request in get_peers
MINSPACE = 0  # min 160 bit space in dht routing table
MAXSPACE = 2**160  # max 160 bit space in dht routing table

# Transversal algorithm
NO_VISITED = 0
VISITED = 1
STALE = -1

BUCKET_TIMER = 900  # 15 minutes timer in bucket
SECRET_TIMER = 300  # New token change every five minutes
SECRET_ACCEPTED = 600  # Token 10 minutes old are accepted

SIZE_ID = 20

DEFAULT_PORT = 6881

# socket
BUFFER_SIZE = 1024
TCP_TYPE = 0
UDP_TYPE = 1
SERVER = 0
CLIENT = 1

# bootstrap addresses

BOOTSTRAP = [("router.bittorrent.com", 6881), ("router.utorrent.com", 6881),
             ("dht.transmissionbt.com", 6881)]  # esta solo devuelve una ip

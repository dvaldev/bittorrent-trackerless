import time
import datetime
from bitTorrent import constants
import threading
from bitTorrent import utils
from bitTorrent import messages
from bitTorrent import sockets
from bitTorrent import bencoding
import socket
import binascii
import random
from bitTorrent import errors


class DHT:
    def __init__(self, my_id):
        self.my_id = my_id
        self.transaction_id = 0
        self.number_of_buckets = []
        self.number_of_buckets.append(1)
        self.token_secret = None
        self.token_accept = []
        self.token = None
        self.generate_secret_for_token()
        self._transaction_id = hex(0)
        # self.accepted_scheduler = None
        # self.accepted_scheduler = threading.Timer(constants.SECRET_ACCEPTED, self.accepted_token)
        # self.accepted_scheduler.start()
        self.bucket = Bucket(constants.MINSPACE, constants.MAXSPACE, my_id, self.number_of_buckets, True)

    def increment_transaction_id(self):
        self._transaction_id = hex(int(self._transaction_id, 16) + 1)

    def generate_secret_for_token(self):
        self.token_secret = ''.join([chr(random.randint(0, 255)) for _ in range(constants.SIZE_ID)])
        secret_scheduler = threading.Timer(constants.SECRET_TIMER, self.generate_secret_for_token)
        secret_scheduler.start()

    def remove_accepted_token(self):
        self.token_accept.remove(self.token)

    def generate_token(self, ip, port, target_info_hash):
        self.token = utils.sha1_string(ip + port + target_info_hash + self.token_secret)
        self.token_accept.append(self.token)
        accepted_scheduler = threading.Timer(constants.SECRET_ACCEPTED, self.remove_accepted_token, [self.token])
        accepted_scheduler.start()

    def bootstrap(self):
        query = messages.message_find_node(self.my_id, self.transaction_id_to_string(), self.my_id)
        self.increment_transaction_id()
        for ip, port in constants.BOOTSTRAP:
            s = sockets.Socket(ip, port, constants.UDP_TYPE, constants.CLIENT)
            s.send(bencoding.encode_element(query))
            r = s.receive()
            response = bencoding.decode_element(r, 0)[0]
            all_nodes = response["r"]["nodes"]
            nodes = utils.split_compact_node_info(all_nodes)
            for n in nodes:
                print("bootstrap", n[0], n[1], n[2])
                node = Node(n[0], n[1], n[2])
                self.add_node(node)

    def ping(self, node):
        query = messages.message_ping(self.my_id, self.transaction_id_to_string())
        self.increment_transaction_id()
        s = sockets.Socket(node.dir_ip, node.port, constants.UDP_TYPE, constants.CLIENT)
        s.send(bencoding.encode_element(query))
        try:
            r = s.receive()
            response = bencoding.decode_element(r, 0)[0]
            if response['y'] != 'e':
                return True
            else:
                raise errors.KrpcError(response)
        except socket.timeout:
            return False
        except errors.KrpcError as e:
            print(e.typeError, e.error)
            return False

    # hacerlo recursivo
    def get_peers(self, node, info_hash):
        print(type(node), node)
        query = messages.message_get_peers(self.my_id, self.transaction_id_to_string(), binascii.unhexlify(info_hash))
        self.increment_transaction_id()
        s = sockets.Socket(node.dir_ip, node.port, constants.UDP_TYPE, constants.CLIENT)
        s.send(bencoding.encode_element(query))
        try:
            r = s.receive()
            response = bencoding.decode_element(r, 0)[0]
            if response['y'] != 'e':
                if 'nodes' in response['r']:
                    if 'token' in response or 'token' in response['r']:
                        print(response)
                        return utils.split_compact_node_info(response['r']['nodes']), response['r']['token'], False
                elif 'values' in response['r']:
                    return utils.split_compact_peer_info(response['r']['values']), response['r']['token'], True
                else:
                    print("No nodes and values in response get_peers")
            else:
                raise errors.KrpcError(response)
        except socket.timeout:
            print("geet peers: socket timeout")
        except errors.KrpcError as e:
            print(e.typeError, e.error)
        return None

    def get_peers_recursive(self, info_hash, nodes_closest):
        # nodes_closest (node, state) -> state = NO_VISITED | VISITED | STALE
        self.sort_guid_by_distance(nodes_closest, info_hash)
        print(len(nodes_closest))
        """queried_responded = 0
        for i in range(constants.K):
            if nodes_closest[i][2] == constants.VISITED:
                queried_responded +=1
        if(queried_responded==constants.K):
            return"""
        queried_responded = 0
        for i in range(len(nodes_closest)):
            if queried_responded == constants.K:
                break
            elif nodes_closest[i][2] == constants.VISITED:
                queried_responded += 1
            elif nodes_closest[i][2] == constants.STALE:
                pass
            elif nodes_closest[i][2] == constants.NO_VISITED:
                break
        if queried_responded == constants.K:
            return
        alpha_count = 0
        for n in nodes_closest:
            if alpha_count < constants.ALPHA:
                if n[2] == constants.NO_VISITED:
                    response = self.get_peers(n[0], info_hash)
                    alpha_count += 1
                    if response is not None:
                        guid = (n[0], n[1], constants.VISITED, utils.xor(n[0].id_node.encode('hex'),
                                                                         info_hash.encode('hex')))
                        nodes_closest.remove(n)
                        nodes_closest.append(guid)
                        if response[2] is False:
                            nodes = response[0]
                            token = response[1]
                            for node in nodes:
                                node_object = Node(node[0], node[1], node[2])
                                nodes_closest.append((node_object, token, constants.NO_VISITED,
                                                      utils.xor(node_object.id_node.encode('hex'),
                                                                info_hash.encode('hex'))))
                        else:
                            values = response[0]
                            print(values)
                    else:
                        # guid = ((n[0],n[1],constants.STALE,
                        # utils.xor(n[0].id_node.encode('hex'),info_hash.encode('hex'))))
                        nodes_closest.remove(n)
                        # nodes_closest.append(guid)
                        print("5", len(nodes_closest))
            else:
                self.get_peers_recursive(info_hash, nodes_closest)

    @staticmethod
    def sort_guid_by_distance(nodes, target):
        nodes.sort(key=lambda guid: utils.xor(guid[0].id_node.encode('hex'), target.encode('hex')))

    # port es entero/ hacerlo recursivo
    def find_node(self, node, info_hash):
        query = messages.message_get_peers(self.my_id, self.transaction_id_to_string(), binascii.unhexlify(info_hash))
        self.increment_transaction_id()
        s = sockets.Socket(node.dir_ip, node.port, constants.UDP_TYPE, constants.CLIENT)
        s.send(bencoding.encode_element(query))
        try:
            r = s.receive()
            response = bencoding.decode_element(r, 0)[0]
            if response['y'] != 'e':
                if 'nodes' in response['r']:
                    return utils.split_compact_node_info(response['r']['nodes'])
            else:
                raise errors.KrpcError(response)
        except socket.timeout:
            print("find_nodes: socket timeout")
        except errors.KrpcError as e:
            print(e.typeError, e.error)

    # creo que se puede ignorar la respuesta
    def announce_peers(self, node, info_hash, token, port):
        query = messages.message_announce_peers(self.my_id, self.transaction_id_to_string(),
                                                binascii.unhexlify(info_hash), token, port)
        self.increment_transaction_id()
        s = sockets.Socket(node.dir_ip, node.port, constants.UDP_TYPE, constants.CLIENT)
        s.send(bencoding.encode_element(query))
        try:
            r = s.receive()
            response = bencoding.decode_element(r, 0)[0]
            if response['y'] != 'e':
                if 'nodes' in response['r']:
                    return utils.split_compact_node_info(response['r']['nodes'])
            else:
                raise errors.KrpcError(response)
        except socket.timeout:
            print("find_nodes: socket timeout")
        except errors.KrpcError as e:
            print(e.typeError, e.error)

    def add_node(self, node):
        if self.ping(node):
            self.bucket.add_node(node)

    def print_routing_table(self):
        self.bucket.print_bucket()
        print(self.number_of_buckets)

    def get_k_closest(self, target_id):
        closest_node = []
        self.bucket.get_k_closest(target_id, closest_node)
        return closest_node

    def transaction_id_to_string(self):
        return '{:x}'.format(int(self._transaction_id, 16))

    @property
    def transaction_id(self):
        return self._transaction_id


class HandleResponses:
    def __init__(self):
        pass

    def handle_response(self):
        pass

    def handle_ping(self):
        pass

    def handle_findnode(self):
        pass

    def handle_getpeers(self):
        pass

    def handle_announce_peers(self):
        pass


class Bucket:
    def __init__(self, min_size, max_size, my_id, number_of_buckets, splittable):
        self.my_id = my_id
        self._min_size = min_size
        self._max_size = max_size
        self._last_changed = time.time()
        self._kbuckets = dict()
        self._count_nodes = 0
        self.scheduler = threading.Timer(constants.BUCKET_TIMER, self.time_out)
        self.scheduler.start()
        self._right_bucket = None
        self._left_bucket = None
        self._number_of_buckets = number_of_buckets
        self.splittable = splittable

    def add_node(self, node):
        if self.bucket_is_leaf():
            if self.bucket_is_not_full():
                if self.node_in_range(node.id_node):
                    self._kbuckets[node.id_node] = node
                    self._count_nodes += 1
            elif self.bucket_can_split():
                self.split_bucket()
                if self._left_bucket.node_in_range(node.id_node):
                    self._left_bucket.add_node(node)
                elif self._right_bucket.node_in_range(node.id_node):
                    self._right_bucket.add_node(node)
            else:
                print("bucket can't split")
        else:
            if self._left_bucket.node_in_range(node.id_node):
                self._left_bucket.add_node(node)
            elif self._right_bucket.node_in_range(node.id_node):
                self._right_bucket.add_node(node)

    def bucket_is_not_full(self):
        if self._count_nodes < constants.K:
            return True
        else:
            return False

    def bucket_is_leaf(self):
        if self._kbuckets is not None:
            return True
        else:
            return False

    def delete_node(self, node):
        del (self._kbuckets[node.id_node])
        self._count_nodes -= 1

    def bucket_can_split(self):
        if (self._max_size - self._min_size) / 2 >= 2 and self.splittable is True:
            return True
        else:
            return False

    """def node_in_range(self, node):
        if self.min_size <= int(utils.xor(node.id_node.encode('hex'), self.my_id.encode('hex')), 16 ) < self.max_size:
            return True
        else:
            return False"""

    def node_in_range(self, id_node):
        if self.min_size <= int(utils.xor(id_node.encode('hex'), self.my_id.encode('hex')), 16) < self.max_size:
            return True
        else:
            return False

    def split_bucket(self):
        new_space = (self._max_size - self._min_size) / 2
        self.number_of_buckets[0] += 1
        self._left_bucket = Bucket(self._min_size, self._min_size + new_space, self.my_id, self.number_of_buckets, True)
        self._right_bucket = Bucket(self._max_size - new_space, self._max_size, self.my_id, self.number_of_buckets,
                                    False)
        for key, value in self._kbuckets.items():
            if self._left_bucket.node_in_range(value.id_node):
                self._left_bucket.add_node(value)
            elif self._right_bucket.node_in_range(value.id_node):
                self._right_bucket.add_node(value)
            else:
                raise Exception
        self._kbuckets.clear()
        self._kbuckets = None
        return self._left_bucket, self._right_bucket

    @property
    def count_nodes(self):
        return self._count_nodes

    @property
    def min_size(self):
        return self._min_size

    @property
    def max_size(self):
        return self._max_size

    @property
    def kbuckets(self):
        return self._kbuckets

    @property
    def number_of_buckets(self):
        return  self._number_of_buckets

    def time_out(self):
        print(datetime.datetime.now())
        self.scheduler = threading.Timer(constants.BUCKET_TIMER, self.time_out)
        self.scheduler.start()

    def print_bucket(self):
        if self._kbuckets is None:
            self._left_bucket.print_bucket()
            self._right_bucket.print_bucket()
        else:
            print("\nSize: " + str(self._kbuckets.__len__()))
            print("Min size : ", self.min_size)
            print("Max size : ", self.max_size)
            print("Nodos\t :")
            for node_id, node in self._kbuckets.items():
                node.print_node()

    """def get_k_closest2(self, node):
        closest_nodes = []
        if self.bucket_is_leaf():
            for key, n in self._kbuckets.items():
                if len(closest_nodes) < constants.K:
                    closest_nodes.append(n)
                    self.sort_list_by_distance(closest_nodes, node)
                else:
                    if closest_nodes[-1] > utils.xor(n.id_node, node):
                        closest_nodes[-1] = n
                        self.sort_list_by_distance(closest_nodes, node)
        else:
            if self._left_bucket.node_in_range(node):
                closest_nodes = self._left_bucket.get_k_closest(node)
            if self._right_bucket.node_in_range(node):
                closest_nodes =self._right_bucket.get_k_closest(node)
        return closest_nodes"""

    def get_k_closest2(self, target_id):
        closest_nodes = []
        if self.bucket_is_leaf():
            for key, n in self._kbuckets.items():
                if len(closest_nodes) < constants.K:
                    closest_nodes.append(n)
                    self.sort_list_by_distance(closest_nodes,
                                               target_id)
                else:
                    if utils.xor(closest_nodes[-1].id_node, target_id) > utils.xor(n.id_node, target_id):
                        closest_nodes[-1] = n
                        self.sort_list_by_distance(closest_nodes, target_id)
        else:
            if self._left_bucket.node_in_range(target_id):
                closest_nodes = self._left_bucket.get_k_closest(target_id)
            if self._right_bucket.node_in_range(target_id):
                closest_nodes = self._right_bucket.get_k_closest(target_id)
        return closest_nodes

    def get_k_closest(self, target_id, closest_nodes):
        if self._kbuckets is None:
            self._left_bucket.get_k_closest(target_id, closest_nodes)
            self._right_bucket.get_k_closest(target_id, closest_nodes)
        else:
            for node_id, node in self._kbuckets.items():
                if len(closest_nodes) < constants.K:
                    closest_nodes.append(node)
                    self.sort_list_by_distance(closest_nodes, target_id)
                else:
                    print(len(closest_nodes), closest_nodes[-1].id_node, node.id_node, node_id)
                    print(utils.xor(closest_nodes[-1].id_node.encode('hex'), target_id))
                    if utils.xor(closest_nodes[-1].id_node.encode('hex'), target_id) > utils.xor(
                            node.id_node.encode('hex'), target_id):
                        print("entra")
                        closest_nodes[-1] = node
                        self.sort_list_by_distance(closest_nodes, target_id)

    """def sort_list_by_distance2(self, nodes, target):
        nodes.sort(key = lambda  node: utils.xor(node.id_node.encode('hex'), target.id_node.encode('hex')))"""

    @staticmethod
    def sort_list_by_distance(nodes, target):
        nodes.sort(key=lambda node: utils.xor(node.id_node.encode('hex'), target.encode('hex')))


class Node:
    def __init__(self, id_node, dir_ip, port):
        self._id_node = id_node
        self._dir_ip = dir_ip
        self._port = port
        self._node_type = NodeType.GOOD  # igual no es Good

    @property
    def id_node(self):
        return self._id_node

    @property
    def dir_ip(self):
        return self._dir_ip

    @property
    def port(self):
        return self._port

    @property
    def node_type(self):
        return self._node_type

    @id_node.setter
    def id_node(self, value):
        self._id_node = value

    @dir_ip.setter
    def dir_ip(self, value):
        self._dir_ip = value

    @port.setter
    def port(self, value):
        self._port = value

    @node_type.setter
    def node_type(self, value):
        self._node_type = value

    def print_node(self):
        print("id node : " + self.id_node.encode('hex') + " - ip addres : " + self.dir_ip + " - ip port : " + str(
            self.port) +
              " - ip node type : " + str(self.node_type))
        # print(utils.hexadecimal_to_binary(self.id_node.encode('hex')))


class NodeType:
    def __init__(self):
        pass

    GOOD, BAD, QUESTIONABLE, UNKNOWN = range(4)

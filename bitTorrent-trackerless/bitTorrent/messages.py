# coding=utf-8
"""Módulo del creación de mensajes para el uso del protocolo DHT

Se pueden enviar los siguientes KRPC (Kademlia Remote Procedure Call):

            - ping.
            - find_node.
            - get_peers.
            - announce_peers.


.. module:: bitTorrent.messages
   :platform: Unix, Windows
   :synopsis: Mensajes de KRPCs.
.. moduleauthor:: Diego J. Valdeolmillos Villaverde

"""
from  bitTorrent import utils

def message_ping(peer_id, transaction_id):
    return {
        'y': 'q',
        't': transaction_id,
        'q': 'ping',
        'a': {'id': peer_id}
    }


def message_find_node(peer_id, transaction_id, node_target):
    return {
        'y': 'q',
        't': transaction_id,
        'q': 'find_node',
        'a': {'id': peer_id, 'target': node_target}
    }


def message_get_peers(peer_id, transaction_id, info_hash):
    return {
        'y': 'q',
        't': transaction_id,
        'q': 'get_peers',
        'a': {'id': peer_id, 'info_hash': info_hash}
    }


def message_announce_peers(peer_id, transaction_id, info_hash, token, port):
    return {
        'y': 'q',
        't': transaction_id,
        'q': 'announce_peers',
        'a': {'id': peer_id, 'info_hash': info_hash, 'port': port, 'token': token}
    }

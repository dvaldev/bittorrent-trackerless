import hashlib
import random
from binascii import unhexlify, hexlify
import string
import struct
import socket


def hexadecimal_to_binary(string_hex):
    h_size = len(string_hex) * 4
    return (bin(int(string_hex, 16))[2:]).zfill(h_size)


def xor(a, b):
    return hexlify(''.join(chr(ord(c1) ^ ord(c2)) for c1, c2 in zip(unhexlify(a[-len(b):]), unhexlify(b))))


def sha1_string(string_to_sha):
    return hashlib.sha1(string_to_sha).hexdigest()


def generate_id():
    # idPeer = ''.join([chr(random.randint(0, 255)) for _ in range(constants.SIZE_ID)])
    id_peer = ''.join([random.choice(string.ascii_lowercase + string.digits) for _ in range(20)])
    return id_peer


"""def integer_ip_to_string(ip):
    segments = []
    for i in range(4):
        segments.append(str((ip << i) & 0xff))
    return ".".join(segments)

def split_peer_string2(peer_string):
    peers = []
    while peer_string != "":
        peer = peer_string[:20]
        print("id hex to string",id_to_hex_string(peer), peer, hex_string_to_id(id_to_hex_string(peer)))
        ip = integer_ip_to_string(struct.unpack(">I", peer_string[20:24])[0])
        port = struct.unpack(">H", peer_string[24:26])[0]
        peer_hexa = peer.encode('hex')
        peers.append((peer, ip, port,hexadecimal_to_binary(peer_hexa)))
        peer_string = peer_string[26:]
    return peers"""


def split_compact_node_info(compact_node_info):  # nodes
    nodes = []
    for i in range(0, len(compact_node_info), 26):
        s = compact_node_info[i:i + 26]
        identifier = s[:20]
        ip = socket.inet_ntop(socket.AF_INET, s[-6:][:4])
        port = struct.unpack(">H", s[-2:])[0]
        print(identifier, ip, port)
        nodes.append((identifier, ip, port))
    return nodes


def split_compact_peer_info(compact_peer_info):  # values
    peers = []
    for i in range(0, len(compact_peer_info), 6):
        s = compact_peer_info[i:i + 6]
        ip = socket.inet_ntop(socket.AF_INET, s[-6:][:4])
        port = struct.unpack(">h", s[-2:])[0]
        print("split compact peer info", ip, port)
        peers.append((ip, port))
    return peers

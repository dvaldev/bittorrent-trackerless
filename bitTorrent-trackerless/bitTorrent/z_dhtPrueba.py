import random
from bitTorrent import dht
from bitTorrent import  utils
from  bitTorrent import constants
import datetime
import time
import codecs
import binascii
import base64
from bitTorrent import errors
from bitTorrent import bencoding


"""id_node = utils.sha1_string(str(123456780))
print(hex(int(id_node)))
print(utils.xor(hex((int(id_node))),hex(int("f84b51f0d2c3455ab5dabb6643b4340234cd036e"))))
routing_table = dht.DHT()

for i in range(9):
    node = dht.Node(utils.sha1_string(str(123456780+i)),"192.168.1.1","5456")
    node.print_node()
    routing_table.bucket.add_node(node)

routing_table.bucket.print_bucket()
"""


"""def test_distance():
    id1 = 730750818665451459101842416358141509827966271491
    node_ids2 = [857081, 279936, 134218071, 3486849955, 0, 4096, 31381059609, 35184372088832]

    routing_table = dht.DHT()
    for id2 in node_ids2:
        n = dht.Node(id2, "127.0.0.1", 8000)
        routing_table.bucket.add_node(n)

    for i in range (8) :
        n = dht.Node(id1+i,"192.168.1.1",7500)
        routing_table.bucket.add_node(n)
    routing_table.print_routing_table()
    n = dht.Node(999,"192.168.1.1",7500)
    routing_table.bucket.add_node(n)
    print(routing_table.print_routing_table())"""

def test_distance():
    my_id = utils.generate_id()
    """print(type(my_id),my_id, codecs.encode(my_id))
    n1 = codecs.encode(my_id)
    n2 = codecs.encode(utils.generate_id())
    print(n1,n2)
    print(int(binascii.hexlify(n1),16) ^ int(binascii.hexlify(n2),16))
    xor_sol = int(binascii.hexlify(n1),16) ^ int(binascii.hexlify(n2),16)
    print (xor_sol, bin(xor_sol), bin(xor_sol).__len__())"""
    #print(utils.xor(binascii.hexlify(n1),binascii.hexlify(n2)))
    #print(utils.xor(codecs.encode(my_id), codecs.encode(utils.generate_id())))
    node_ids = [0b0001,0b0010, 0b0011, 0b0100, 0b0101, 0b0110, 0b1000, 0b1001,
                 0b1010, 0b1011, 0b1100, 0b1101, 0b1110, 0b1111]
    port = 8001
    routing_table = dht.DHT(my_id)
    routing_table.bootstrap()
    routing_table.print_routing_table()
    node = dht.Node("5990fdebbbb8425a69abec746360a8c5305ef1fe", "127.0.0.1", "6558")
    nodes = routing_table.get_k_closest(node)
    for n in nodes:
        #routing_table.get_peers(n,"f84b51f0d2c3455ab5dabb6643b4340234cd036e")
        routing_table.ping(n)

    """
    prueba closest nodes para un id dado

    node = dht.Node("5990fdebbbb8425a69abec746360a8c5305ef1fe", "127.0.0.1", "6558")
    nodes = routing_table.get_k_closest(node)
    int_nodes = []
    for n in nodes:
        int_nodes.append(utils.xor(n.id_node.encode("hex"),"5990fdebbbb8425a69abec746360a8c5305ef1fe".encode("hex") ))
        #print(n.id_node.encode("hex"), int(n.id_node.encode("hex"),16 ))
    #int_nodes.sort()
    print("")


    int_nodes.sort()
    print("")
    j = 0
    for i in int_nodes:
        j+=1
        print(j)
        print(len(utils.hexadecimal_to_binary(i)), utils.hexadecimal_to_binary(i))
    """

    """for i in range(127):
        n = dht.Node(utils.generate_id(), "127.0.0.1", port+i)
        routing_table.bucket.add_node(n)
    routing_table.print_routing_table()"""

def test_generate_token():
    my_id = utils.generate_id()
    routing_table = dht.DHT(my_id)
    while True:
        routing_table.generate_token("192.168.1.1", "6881","5990fdebbbb8425a69abec746360a8c5305ef1fe")
        time.sleep(3)

def test_rotate_transaction_id():
    my_id = utils.generate_id()
    routing_table = dht.DHT(my_id)
    while True:
        routing_table.rotate_transaction_id()
        print(routing_table.transaction_id_to_string())
        time.sleep(2)


def test_krpc_error():
    bdictionary = "d1:eli201e23:A Generic Error Ocurrede1:ti0e1:y1:ee"
    dictionary = bencoding.decode_element(bdictionary,0)[0]
    try:
        raise errors.KrpcError(dictionary)
    except errors.KrpcError as e:
        print (e.typeError, e.error)

def test_guid():
    g = []
    g.append((True,False,True))
    for i in g:
        print(i[0])
        print(i[1])
        print(i[2])

def test_get_peers():
    my_id = utils.generate_id()
    infohash = "5990fdebbbb8425a69abec746360a8c5305ef1fe"
    routing_table = dht.DHT(my_id)
    routing_table.bootstrap()
    nodes = routing_table.get_k_closest(infohash)
    response = routing_table.get_peers(nodes[0],infohash)
    print ("response", type(response), len(response[0]), response[1])
    print(response)
    print(response[2])

def test_get_peers_recursive():
    my_id = utils.generate_id()
    infohash = "c99afdebbb18425a69abec746360a8c5305ef12e"
    routing_table = dht.DHT(my_id)
    routing_table.bootstrap()
    routing_table.print_routing_table()
    nodes_closest = routing_table.get_k_closest(infohash)
    closest  = []
    print(len(nodes_closest))
    for n in nodes_closest:
        print(utils.hexadecimal_to_binary(n.id_node.encode('hex')))
        closest.append((n,"",constants.NO_VISITED, utils.xor(n.id_node.encode('hex'),infohash.encode('hex'))))
    print("ANTES:", len(closest))
    for n in closest:
        n[0].print_node()
    routing_table.get_peers_recursive(infohash,closest)
    print("DESPUES",len(closest))
    for n in closest:
        print(n[0].id_node.encode('hex'), n[1], n[2])
    print("\n")
    for n in closest:
        #print(utils.hexadecimal_to_binary(n[0].id_node.encode('hex')), n[1],n[2])
        print(utils.hexadecimal_to_binary(n[3]),n[1])

print("dhtPrueba")
print(datetime.datetime.now())

#test_get_peers()
test_get_peers_recursive()
#test_krpc_error()
#test_generate_token()
#test_distance()
#test_rotate_transaction_id()




#test_guid()


from bitTorrent import sockets
from bitTorrent import constants
import socket
HOST = '127.0.0.1'                 # Symbolic name meaning all available interfaces
PORT = 50011              # Arbitrary non-privileged port

s = sockets.Socket(HOST,PORT, constants.TCP_TYPE, constants.SERVER)
while 1:
    data = s.receive()
    print(data)
    if not data: break
    s.send(data+" recibido")
s.close()
print("socket closed")


# coding=utf-8
"""Bencoding es la codificación utilizada por el protocolo bitTorrent para guardar y transmitir
datos de forma estructurada.

Se pueden codificar los siguientes tipos:

            - enteros (int).
            - cadenas (str).
            - listas  (list).
            - diccionarios (dict).



.. module:: bitTorrent.bencoding
   :platform: Unix, Windows
   :synopsis: Codificación y decodificación en formato Bencoding.
.. moduleauthor:: Diego J. Valdeolmillos Villaverde

"""


def encode_string(string):
    """Codifica una cadena en formato Bencoding.

    Las cadenas de texto tienen un prefijo con la longitud de la cadena seguida de dos puntos y la cadena.

    :param string: Cadena a codificar.
    :type string: str.
    :returns:  **str** -- cadena codificada.

    **Ejemplo:**

    >>> encode_string('python',0)
    '6:python'
    >>> encode_string('tfg',0)
    '3:tfg'

    """
    return str(len(string)) + ':' + string


def encode_integer(integer):
    """Codifica un entero en formato Bencoding

    Los números enteros están representados por una “i” seguida por un número seguido de una “e”.

    :param integer: Número entero a codificar.
    :type integer: int.
    :returns:  **str** -- cadena codificada.

    **Ejemplo:**

    >>> encode_integer(32,0)
    'i32e'
    >>> encode_integer(-32,0)
    'i-32e'

    """
    return 'i' + str(integer) + 'e'


def encode_list(l):
    """Codifica una lista en formato Bencoding

    Las listas están codificadas con una "l" seguida por sus elementos también codificados seguidos de una "e".

    :param l: Lista a codificar.
    :type l: list.
    :returns:  **str** -- cadena codificada.

    **Ejemplo:**

    >>> encode_list(['spam', 32],0)
    'l4:spami32ee'
    >>> encode_list([],0)
    'le'

    """
    s = "l"
    for item in l:
        s += encode_element(item)
    return s + 'e'  # string


def encode_dictionary(dictionary):
    """Codifica un diccionario en formato Bencoding

    Los diccionarios están codificados con una d seguida de una lista de claves alternativas y sus
    correspondientes valores seguidos por una e.

    :param dictionary: Diccionario a codificar.
    :type dictionary: dict.
    :returns:  **str** -- cadena codificada.

    **Ejemplo:**

    >>> encode_dictionary({'cow': 'moo', 'spam': ['eggs', 44]},0)
    'd3:cow3:moo4:spaml4:eggsi44eee'
    >>> encode_dictionary({},0)
    'de'

    """
    s = "d"
    for key, value in dictionary.items():
        s += encode_element(key) + encode_element(value)
    return s + "e"  # string


def encode_element(element):
    """Codifica un elemento en formato Bencoding.
    Recibe un elemento (str, int, list, dict) y resuelve su tipo para codificarlo.

    :param element: elemento a codificar.
    :type element: str. int. list. dict.
    :returns:  **str** -- cadena codificada.
    :raise:  Invalid element.

    **Ejemplo:**

    >>> encode_element(32,0)
    'i32e'
    >>> encode_element(-32,0)
    'i-32e'
    >>> encode_element('python',0)
    '6:python'
    >>> encode_element('tfg',0)
    '3:tfg'
    >>> encode_element(['spam', 32],0)
    'l4:spami32ee'
    >>> encode_element([],0)
    'le'
    >>> encode_element({'cow': 'moo', 'spam': ['eggs', 44]},0)
    'd3:cow3:moo4:spaml4:eggsi44eee'
    >>> encode_element({},0)
    'de'

    """
    if type(element) is int:
        return encode_integer(element)
    elif type(element) is str:
        return encode_string(element)
    elif type(element) is list:
        return encode_list(element)
    elif type(element) is dict:
        return encode_dictionary(element)
    raise ValueError("Invalid element -> " + str(type(element)))


def decode_dictionary(string, index):
    """Decodifica una cadena en formato Bencoding a diccionario

    Los diccionarios están codificados con una d seguida de una lista de claves alternativas y sus
    correspondientes valores seguidos por una e.

    :param string: Cadena a decodificar.
    :param index: Índice para recorrer la cadena a decodificar.
    :type string: str.
    :type index: int.
    :returns: **tuple** -- Tupla que contiene:

                  - **dict**  --  Diccionario decodificado.
                  - **int**  -- Índice para recorrer la cadena.

    **Ejemplo:**

    >>> decode_dictionary('d3:cow3:moo4:spaml4:eggsi44eee',0)
    ({'cow': 'moo', 'spam': ['eggs', 44]}, 30)
    >>> decode_dictionary('de',0)
    ({}, 2)

    """
    dictionary = dict()
    if string[index] == 'd':
        index += 1
    while string[index] != 'e':
        key, index = decode_element(string, index)
        value, index = decode_element(string, index)
        dictionary[key] = value
    index += 1
    return dictionary, index


def decode_list(string, index):
    """Decodifica una cadena en formato Bencoding a lista

    Las listas están codificadas con una "l" seguida por sus elementos también codificados seguidos de una "e".

    :param string: Cadena a decodificar.
    :param index: Índice para recorrer la cadena a decodificar.
    :type string: str.
    :type index: int.
    :returns: **tuple** -- Tupla que contiene:

            - **list** --  lista decodificado.
            - **int** -- Índice para recorrer la cadena.

    **Ejemplo:**

    >>> decode_list('l4:spami32ee',0)
    (['spam', 32], 12)
    >>> decode_list('le',0)
    ([], 2)

    """
    l = list()
    print(string)
    if string[index] == 'l':
        index += 1
    while string[index] != 'e':
        item, index = decode_element(string, index)
        l.append(item)
    index += 1
    return l, index


def decode_string(string, index):
    """Decodifica una cadena en formato Bencoding a cadena

    Las cadenas de texto tienen un prefijo con la longitud de la cadena seguida de dos puntos y la cadena.

    :param string: Cadena a decodificar.
    :param index: Índice para recorrer la cadena a decodificar.
    :type string: str.
    :type index: int.
    :returns: **tuple** -- Tupla que contiene:

            - **str** --  Cadena decodificado.
            - **int** -- Índice para recorrer la cadena.

    **Ejemplo:**

    >>> decode_string('6:python',0)
    ('python', 8)
    >>> decode_string('3:tfg',0)
    ('tfg', 5)

    """
    s = string[index:]
    string_length = ""
    i = 0
    for i, c in enumerate(s):
        if c == ':':
            break
        string_length += c
    index += i + 1
    decode = string[index:index + int(string_length)]
    index += int(string_length)
    return decode, index


def decode_integer(string, index):
    """Decodifica una cadena en formato Bencoding a número entero

    Los números enteros están representados por una “i” seguida por un número seguido de una “e”.

    :param string: Cadena a decodificar.
    :param index: Índice para recorrer la cadena a decodificar.
    :type string: str.
    :type index: int.
    :returns: **tuple** -- Tupla que contiene:

            - **int** --  Entero decodificado.
            - **int** -- Índice para recorrer la cadena.

    **Ejemplo:**

    >>> decode_integer('i32e',0)
    (32, 4)
    >>> decode_integer('i-32e',0)
    (-32, 5)

    """
    index += 1
    s = string[index:]
    st = ""
    i = 0
    for i, c in enumerate(s):
        if c == 'e':
            break
        st += c
    index += i + 1
    return int(st), index


def decode_element(string, index):
    """Decodifica una cadena en formato Bencoding a elemento.

    Un elemento puede ser:


        - entero (int).
        - cadena (str).
        - lista (list).
        - diccionario (dict).

    :param string: Cadena a decodificar.
    :param index: Índice para recorrer la cadena a decodificar.
    :type string: str.
    :type index: int.
    :returns: **tuple** -- Tupla que contiene el elemento decodificado.

            - **int | str | list | dict** --  Elemento decodificado.
            - **int** -- Índice para recorrer la cadena.

    **Ejemplo:**

    >>> decode_element('i32e',0)
    (32, 4)
    >>> decode_element('i-32e',0)
    (-32, 5)
    >>> decode_element('6:python',0)
    ('python', 8)
    >>> decode_element('3:tfg',0)
    ('tfg', 5)
    >>> decode_element('l4:spami32ee',0)
    (['spam', 32], 12)
    >>> decode_element('le',0)
    ([], 2)
    >>> decode_element('d3:cow3:moo4:spaml4:eggsi44eee',0)
    ({'cow': 'moo', 'spam': ['eggs', 44]}, 30)
    >>> decode_element('de',0)
    ({}, 2)

    """
    if string[index] == 'd':
        return decode_dictionary(string, index)
    elif string[index] == 'l':
        return decode_list(string, index)
    elif string[index] == 'i':
        return decode_integer(string, index)
    elif string[index].isdigit():
        return decode_string(string, index)
    raise ValueError("Invalid String -> " + string[index])

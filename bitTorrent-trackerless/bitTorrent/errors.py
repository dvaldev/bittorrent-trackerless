# coding=utf-8
"""Modulo utilizado para contener errores específicos para ser tratados por excepciones.

.. module:: bitTorrent.errors
   :platform: Unix, Windows
   :synopsis: Tratamiento de errores.
.. moduleauthor:: Diego J. Valdeolmillos Villaverde

"""


class KrpcError(Exception):
    """Clase específica si se reciben un mensaje con la clave 'e' de otros peers.
    """
    def __init__(self, dictionary):
        self.dictionary = dictionary
        self.typeError = dictionary['e'][0]
        self.error = dictionary['e'][1]

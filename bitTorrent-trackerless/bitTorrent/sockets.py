# coding=utf-8
"""Módulo de creación de sockets TCP (SOCK_STREAM) o UDP (SOCK_DGRAM)

Se usan los siguientes métodos
            - send.
            - receive.
            - close.

.. module:: bitTorrent.sockets
   :platform: Unix, Windows
   :synopsis: Sockets.
.. moduleauthor:: Diego J. Valdeolmillos Villaverde

"""
import socket
from bitTorrent import constants


class Socket:
    """Clase de creación  de sockets SOCK_STREAM o SOCK_DGRAM. Contiene los siguiente métodos:
        - __init__
        - send
        - receive
        - close
    """
    def __init__(self, ip_dst, port_dst, socket_type, rol):
        """Crea el socket.

        :param ip_dst: Dirección ip.
        :param port_dst: Puerto.
        :param socket_type: tipo de socket (TCP o UDP) (0 o 1).
        :param rol: rol (servidor o cliente)(0 o 1).
        :type ip_dst: str.
        :type port_dst: int.
        :type socket_type: int.
        :type rol: int.

        **Ejemplo:**

        >>> Socket('127.0.0.1',6881, constants.TCP, constants.SERVER)
        'Servidor TCP'
        >>> Socket('127.0.0.1',6882, constants.UDP, constants.CLIENT)
        'Cliente UDP'

        """
        self.address_src = (socket.gethostbyname(ip_dst), port_dst)
        self.address_dst = (socket.gethostbyname(ip_dst), port_dst)
        print(self.address_dst, self.address_src)
        self.family = socket.AF_INET
        if socket_type == constants.TCP_TYPE:
            self.socket_type = socket.SOCK_STREAM
        elif socket_type == constants.UDP_TYPE:
            self.socket_type = socket.SOCK_DGRAM
        self.rol = rol
        self.s = socket.socket(self.family, self.socket_type)
        if self.rol == constants.SERVER:
            self.s.bind(self.address_src)
            if self.socket_type == socket.SOCK_STREAM:
                self.s.listen(1)
                # máximo número de conexiones hechas al socket, en principio 1, puesto que habrá un socket por petición?
                self.connection, self.address_dst = self.s.accept()
                print('Connected by ' + str(self.address_dst))
        elif self.rol == constants.CLIENT:
            if self.socket_type == socket.SOCK_STREAM:
                self.s.connect(self.address_dst)
        else:
            raise ValueError("Rol Invalido -> (Servidor - 0, Cliente - 1) ")

    def send(self, data):
        if self.socket_type == socket.SOCK_STREAM:
            if self.rol == constants.SERVER:
                self.connection.send(data)
            else:
                self.s.send(data)
        else:
            self.s.sendto(data, self.address_dst)
            self.s.settimeout(0.5)

    def receive(self):
        if self.socket_type == socket.SOCK_STREAM:
            if self.rol == constants.SERVER:
                data = self.connection.recv(constants.BUFFER_SIZE)
            else:
                data = self.s.recv(constants.BUFFER_SIZE)
        else:
            try:
                data, self.address_dst = self.s.recvfrom(constants.BUFFER_SIZE)
            except socket.timeout:
                raise socket.timeout
        return data

    def close(self):
        self.s.close()

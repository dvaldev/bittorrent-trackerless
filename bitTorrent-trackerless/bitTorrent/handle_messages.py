# coding=utf-8
"""Módulo del manejo de mensajes procedentes del protocolo DHT

Se pueden recibir los siguientes KRPC (Kademlia Remote Procedure Call):

            - ping.
            - find_node.
            - get_peers.
            - announce_peers.


.. module:: bitTorrent.handle_messages
   :platform: Unix, Windows
   :synopsis: Manejadora de KRPCs.
.. moduleauthor:: Diego J. Valdeolmillos Villaverde

"""
from bitTorrent import bencoding



def handle_messages(message, type_message, token):
    decode_message, index = bencoding.decode_element(message, 0)
    if token != decode_message['t']:
        print('error')
        return -1
    if type_message == 'ping':
        handle_ping(decode_message)
    elif type_message == 'find_node':
        handle_find_node(decode_message)
    elif type_message == 'get_peers':
        handle_get_peers(decode_message)
    elif type_message == 'announce_peers':
        handle_announce_peer(decode_message)
    else:
        return -1


def handle_ping(message):
    pass

def handle_find_node(message):
    pass

def handle_get_peers(message):
    pass

def handle_announce_peer(message):
    pass
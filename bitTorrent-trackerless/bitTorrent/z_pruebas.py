from bitTorrent import bencoding
import random
import socket
from  bitTorrent import messages
from  bitTorrent import sockets
from bitTorrent import constants
import struct
import binascii
import struct

rand = lambda: ''.join([chr(random.randint(0, 255)) for _ in range(20)])
my_id = rand()

def query(ip, port):
    print("Trying %s:%d" % (ip, port))
    get_peers = {"t":'0f', "y":"q", "q":"get_peers",
        "a": {"id":my_id,
              "info_hash": '\xd9\x9d\x14\x8c\xf7\xb5\xee'
                           '\x84</\x806\xd6d\x13\xb3\xe3\x0f\x1b\xe7'}
    }
    get_peers_bencoded = bencoding.encode_element(get_peers)

    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.sendto(get_peers_bencoded, (ip, port))
    s.settimeout(0.5)
    try:
        r = s.recvfrom(1024)
    except socket.timeout:
        return []
    response2 = bencoding.decode_element(r[0],0)

    print(response2[0])
    response = response2[0]
    ret = []
    for i in range(0, len(response['r']['nodes']), 26):
        s = response['r']['nodes'][i:i+26]
        ip = socket.inet_ntop(socket.AF_INET, s[-6:][:4])
        port = struct.unpack(">H", s[-2:])[0]
        ret += [(ip, port)]
    print("Got %d nodes." % len(ret))
    return ret

def query2(ip, port):
    ping = messages.message_ping(my_id,'0f')
    ping_bencoded = bencoding.encode_element(ping)
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.sendto(ping_bencoded, (ip, port))
    s.settimeout(0.5)
    try:
        r = s.recvfrom(1024)
        print("--------------->", r)
    except socket.timeout:
        print("socket timetout\n")

def query3(ip, port):
    ping = messages.message_ping(my_id,'0f')
    ping_bencoded = bencoding.encode_element(ping)
    s = sockets.Socket(ip, port, constants.UDP_TYPE, constants.CLIENT)
    #s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    #s.sendto(ping_bencoded, (ip, port))
    #s.settimeout(0.5)
    s.send(ping_bencoded)
    r = s.receive()
    print("--------------->", r)




if __name__ == '__main__':
    """ips = [(socket.gethostbyname('router.bittorrent.com'), 6881)]
    while True:
        node = ips.pop()
        ip, port = node[0], node[1]
        ips += query(ip, port)
        query3(ip,port)
       """
    print(len("79b193385de5b967175ca86073670fce0e45a324"))
    info_hash ="5990fdebbbb8425a69abec746360a8c5305ef1fe"
    print(len(info_hash), binascii.unhexlify(info_hash))


\select@language {spanish}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{5}{chapter.1}
\contentsline {chapter}{\numberline {2}Objetivos del proyecto}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}Objetivos del software}{7}{section.2.1}
\contentsline {section}{\numberline {2.2}Objetivos personales}{7}{section.2.2}
\contentsline {chapter}{\numberline {3}Conceptos te\IeC {\'o}ricos}{9}{chapter.3}
\contentsline {section}{\numberline {3.1}Redes P2P}{9}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Concepto de P2P}{9}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Desaf\IeC {\'\i }os en la implementaci\IeC {\'o}n de aplicaciones P2P}{10}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Arquitectura}{10}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}Historia de las redes P2P}{10}{subsection.3.1.4}
\contentsline {subsection}{\numberline {3.1.5}Aplicaciones}{10}{subsection.3.1.5}
\contentsline {section}{\numberline {3.2}Kademlia}{10}{section.3.2}
\contentsline {section}{\numberline {3.3}BitTorrent}{10}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Protocolo BitTorrent}{10}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Protocolo Distributed Hash Table}{10}{subsection.3.3.2}
\contentsline {chapter}{\numberline {4}Herramientas y t\IeC {\'e}cnicas}{11}{chapter.4}
\contentsline {section}{\numberline {4.1}T\IeC {\'e}cnicas}{11}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Scrum}{11}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Lenguajes de programaci\IeC {\'o}n}{11}{subsection.4.1.2}
\contentsline {subsubsection}{Python}{11}{section*.2}
\contentsline {subsection}{\numberline {4.1.3}Herramientas de documentaci\IeC {\'o}n}{11}{subsection.4.1.3}
\contentsline {subsubsection}{\LaTeX }{11}{section*.3}
\contentsline {subsection}{\numberline {4.1.4}Sistema de control de versiones}{11}{subsection.4.1.4}
\contentsline {subsubsection}{Bitbucket}{11}{section*.4}
\contentsline {section}{\numberline {4.2}Herramientas de desarrollo}{11}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}PyCharm}{11}{subsection.4.2.1}
\contentsline {chapter}{\numberline {5}Aspectos relevantes del desarrollo del proyecto}{13}{chapter.5}
\contentsline {section}{\numberline {5.1}Ciclo de vida}{14}{section.5.1}
\contentsline {section}{\numberline {5.2}Roles}{14}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Scrum Master}{14}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Product Owner}{14}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Equipo de trabajo}{14}{subsection.5.2.3}
\contentsline {section}{\numberline {5.3}Sprint 0}{14}{section.5.3}
\contentsline {section}{\numberline {5.4}Sprints}{14}{section.5.4}
\contentsline {section}{\numberline {5.5}Artefactos}{14}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}Product Backlog}{14}{subsection.5.5.1}
\contentsline {subsubsection}{Historia de Usuario}{14}{section*.5}
\contentsline {subsubsection}{\IeC {\'E}pica}{14}{section*.6}
\contentsline {subsubsection}{Tema}{14}{section*.7}
\contentsline {subsection}{\numberline {5.5.2}Sprint Backlog}{14}{subsection.5.5.2}
\contentsline {subsubsection}{Historia de Usuario}{14}{section*.8}
\contentsline {subsubsection}{Tema}{14}{section*.9}
\contentsline {section}{\numberline {5.6}Sprint Review}{14}{section.5.6}
\contentsline {chapter}{\numberline {6}Trabajos relacionados}{15}{chapter.6}
\contentsline {chapter}{\numberline {7}Conclusiones y l\IeC {\'\i }neas de trabajo futuras}{17}{chapter.7}
\contentsline {chapter}{Glosario}{19}{section*.10}
